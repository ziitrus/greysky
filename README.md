# greysky

---

This project is a bicycle commute dashboard, you can upload your bicycle gps file and analyse some data on those.

---

## Look

### Home

![Home](./docAssets/home.png)

### Upload

![Upload](./docAssets/upload.png)

### Route

![Route](./docAssets/route.png)

## Why ?

To manage your own data and be master of your web identity.

## How ?

This interface goal to be self hosted, but to be realistic people don't do this effort.
so I am currently thinking about another system, to host data and permit to people to keep their privacy.

## How to dev on it

To get a perfect harmony between my api and my frontend, I have create an alias host

I don't know if it's the best practice and be really happy to know how to do this otherwise

```bash
sudo nano /etc/hosts

- add

127.0.0.1       dojo.greysky
```

### Front-end
 - npm install
 - npm start
 - npm watch-css

### Back-end

I am learning python so, I'll be glad to receive any best practice about what I do.

requirements:
- virtualenv
- python

```bash
cd backend
virtualenv <your virtual environment name>
. <virtualenv>/bin/activate
pip install -r requirements.txt

python views/route.py
```

## Todo

- [ ] Do a logo (this is all the thing)
- [ ] Supports multiple format (.fit, .kml, .gpx)
- [ ] Do authentication system, to do users accounts
- [ ] Searching the best way to give people the control of their data
- [ ] Do a user friendly dashboard
- [ ] Trying to use redux, to get a stateless application, and be more efficient on serving the datas
- [ ] Learn infrastructure issues to host the smarter as possible the application
