import React from 'react'

import ListElement from '../../components/ListElement'

class Home extends React.Component {
  render() {
    return (
      <div className="container--view home">
        <ListElement/>
        {this.props.children}
      </div>
    )
  }
}

export default Home
