import React from 'react'
import Input from '../../components/Input'
import { Link } from '@reach/router'
import './upload.css'

class Upload extends React.Component {
  render() {
    return (
      <div className="container--view upload">
        <Link to="/" className="container--icon close--button">close</Link>
        <Input/>
      </div>
    )
  }
}

export default Upload
