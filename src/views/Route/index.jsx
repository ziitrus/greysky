import React from 'react'
import ReactChartkick, {LineChart} from 'react-chartkick'
import Map from '../../components/Map'
import Chart from 'chart.js'

import './route.css'

class Route extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      route: null,
      dataArray: []
    }
  }

  componentDidMount() {
    this.getRoute()
  }

  getRoute() {
    fetch(`http://dojo.greysky:5000/route/${this.props.id}`).then(r => r.json()).then(r => {
      let speed = r.data.reduce((a, i) => {
        a.push([
          Math.floor(i.distance / 1000).toFixed(2),
          Math.floor(i.speed * 3.6)
        ])
        return a
      }, [])
      let altitude = r.data.reduce((a, i) => {
        a.push([
          Math.floor(i.distance / 1000).toFixed(2),
          Math.floor(i.altitude)
        ])
        return a
      }, [])
      let sa = r.data.reduce((a, i) => {
        if (i.speed * 3.6 > 0) {
          a.push(i.speed * 3.6)
        }
        return a;
      }, [])

      this.setState({
        route: r,
        speedArray: speed,
        altitudeArray: altitude,
        maxSpeed: Math.max(...sa),
        time: this.getTime(r.data[r.data.length - 1].timestamp.$date, r.data[0].timestamp.$date),
        distance: (r.data[r.data.length - 1].distance / 1000).toFixed(2)
      })
    })
  }

  getTime(end, start) {
    start = new Date(start).getTime()
    end = new Date(end).getTime()
    let diff = end - start
    return diff
  }

  getDataArray() {
    return this.state.route.data;
  }

  getAverageSpeed(data) {
    let average = data.reduce((a, i) => {
      a.push(i.speed)
      return a
    }, [])
    var total = 0;
    for (var i = 0; i < average.length; i++) {
      total += average[i];
    }
    var avg = total / average.length;
    return (avg * 3.6).toFixed(2)
  }

  render() {
    let route = <div></div>
    let table = <div></div>
    if (this.state.route == null) {} else {
      table = (<table>
        <thead>
          <tr>
            <th>{this.state.distance}
              Km</th>
            <th></th>
          </tr>
          <tr>
            <th></th>
            <th>Speed</th>
            <th>Distance</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Average</td>
            <td>{this.getAverageSpeed(this.state.route.data)}
              km/h</td>
            <td></td>
          </tr>
          <tr>
            <td>Max</td>
            <td>{this.state.maxSpeed.toFixed(2)}
              km/h</td>
            <td></td>
          </tr>
        </tbody>
      </table>)
      route = (<div>
        <h1>{this.state.route.name}</h1>
        {table}
        <Map data={this.state.route.data}/>
      </div>)
    }
    return (<div className="container--view route">
      {route}
      <h2>Speed</h2>
      <LineChart data={this.state.speedArray} stacked={true} spanGaps={true}/>
      <h2>Altitude</h2>
      <LineChart data={this.state.altitudeArray} stacked={true} spanGaps={true}/>
    </div>)
  }
}

export default Route
