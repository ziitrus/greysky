import React from 'react'
import ReactDOM from 'react-dom'
import L from 'leaflet'
import { Link } from '@reach/router'

import ReactChartkick, { AreaChart } from 'react-chartkick'
import Chart from 'chart.js'

import './map.css'

class Map extends React.Component {
    constructor(props) {
      super(props)
      console.log(props)
      this.state = {
        map:{},
        total: {}
      }
    }

    componentDidMount(){
      this.createMap(ReactDOM.findDOMNode(this))
    }

    createMap(element) {
        var map = L.map(element);
        L.tileLayer('https://{s}.tile.openstreetmap.se/hydda/full/{z}/{x}/{y}.png').addTo(map);
        this.setState({map: map})
        this.setupMap(map)

          console.log(this.props)
    }

    setupMap(map){
      map.setView([51.505, -0.09], 13)
      this.drawSegment(map)
    }

    drawSegment(map) {
      let LatLng = this.props.data.reduce((a,i,o,u) => {
        a.push([i.position_lat, i.position_long])
        return a
      }, [])
      var polyline = L.polyline(LatLng, {
        opacity: 0.5,
        className: 'map--line'
      }).addTo(map);
      // zoom the map to the polyline
      map.fitBounds(polyline.getBounds());
      this.setState({
        total: this.props.data[this.props.data.length-1]
      })
    }

    getAverageSpeed() {
      let data = this.props.data
      let average = data.reduce((a,i) => {
        a.push(i.speed)
        return a
      }, [])
      var total = 0;
      for(var i = 0; i < average.length; i++) {
          total += average[i];
      }
      var avg = total / average.length;
      return (avg*3.6).toFixed(2)
    }

    renderLink() {
      return (
        <Link to={'r/'+this.props._id} className="map--info">
          <span className="container--icon">cross</span>
          <h1>{this.props.name}</h1>
          <div>
            Average: {this.getAverageSpeed()} Km/h
          </div>
          <div>
            Distance: {(this.state.total.distance/1000).toFixed(2)} Km
          </div>
        </Link>
      )
    }

    render () {
        return (
          <div className="map">
            { this.props.name ? this.renderLink() : ''}
          </div>
        );
    }

};

export default Map
