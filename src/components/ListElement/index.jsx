import React from 'react'
import Map from '../../components/Map'
import './list_element.css'

class ListElement extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      routes: []
    }
  }
  componentDidMount() {
    this.RoutesList();
  }

  RoutesList() {
    fetch('http://dojo.greysky:5000/routes', {mode: 'cors'})
    .then(res => res.json())
    .then((res) => {
      let computedRes = JSON.parse(res.data).map(r=>{
        r["_id"] = r["_id"].$oid
        return r
      });
      this.setState({
        routes: computedRes
      })
    })
  }
  render() {
    const routes = this.state.routes.map((item, i) => (
      <li key={i} className="cards">
        <Map lat="60.0" lon="10.0" zoom="10" _id={item._id} data={item.data} name={item.name} author={item.author}/>
      </li>
    ))
    return (<ul>
      {routes}
    </ul>)
  }
}

export default ListElement
