import React from 'react'
import './input.css'

class Input extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      file: {},
      username: '',
      loading:false
    }

    this.handleFile = this.handleFile.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleUserName = this.handleUserName.bind(this);
    this.handleRouteName = this.handleRouteName.bind(this);
  }

  handleSubmit(e) {
    e.preventDefault()
    this.setState({loading:true})
    var formData = new FormData();
    formData.append('user', this.state.username)
    formData.append('route_name', this.state.name)
    formData.append('file', this.state.file, 'sample.fit');
    fetch('http://dojo.greysky:5000/insert', {
      method: 'POST',
      mode: 'no-cors',
      body: formData
    })
    .then(r => {
      this.setState({loading:true})
    })
  }

  handleFile(e) {
    const reader = new FileReader();
    const file = e.target.files[0];
    reader.onload = (upload) => {
      this.setState({
        file: file
      })
    };

    reader.readAsDataURL(file)
  }

  handleUserName(e) {
    e.preventDefault()
    this.setState({
      username: e.target.value
    })
  }

  handleRouteName(e) {
    e.preventDefault()
    this.setState({
      name: e.target.value
    })
  }

  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit} encType="multipart/form-data" className="container--form">
          <input type="text" onChange={this.handleUserName} placeholder="username" className="container--form__text"/>
          <input type="text" onChange={this.handleRouteName} placeholder="route name" className="container--form__text"/>
          <input type="file" onChange={this.handleFile} className="container--form__file"/>
          <input type="submit" disabled={!this.state.name} className="container--form__button container--icon" value="rocket"/>
        </form>
      </div>
    )
  }
}

export default Input
