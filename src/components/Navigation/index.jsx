import React from 'react'
import { Link } from '@reach/router'
import './navigation.css'

class Navigation extends React.Component {
  render() {
    return (
      <nav className="navigation">
        <Link to="upload" className="navigation--item">
          Upload{" "}
          <span className="container--icon">arrow-up2</span>
        </Link>
      </nav>
    )
  }
}

export default Navigation
