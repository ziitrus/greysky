import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import '../node_modules/leaflet/dist/leaflet.css'
import {Router, Link} from '@reach/router'

//views
import Home from './views/Home'
import Upload from './views/Upload'
import Route from './views/Route'
import Navigation from './components/Navigation'

class App extends React.Component {
  render() {
    return (<div className="container">
      <Link className="title" to="/">GreySky</Link>
      <Navigation/>
      <Router>
        <Home path="/">
          <Upload path="upload"/>
        </Home>
        <Route path="r/:id"/>
      </Router>
    </div>)
  }
}

ReactDOM.render(<App/>, document.getElementById('root'));
