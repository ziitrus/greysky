from flask import Flask, jsonify, request
from bson.json_util import dumps
from bson.objectid import ObjectId
from fitparse import FitFile
from pymongo import MongoClient
from flask_cors import CORS
from mongo_client import *

app = Flask(__name__)
CORS(app)
static_url_path='static/'
routes = MongoClient('localhost', 27017).greysky.route

@app.route("/insert", methods=['POST'])
def insert_route():
    record_array = []
    fitfile = FitFile(request.files['file'])
    for record in fitfile.get_messages('record'):
        tmp = {}
        for record_data in record:
            tmp[record_data.name] = record_data.value
            if record_data.name == 'position_lat' or record_data.name == 'position_long':
                 tmp[record_data.name] = int(record_data.value) * ( 180 / 2**31 )
        record_array.append(tmp)
    insert_id = insert_route_database({
        "name": request.form.get('route_name'),
        "author": request.form.get('user'),
        "data": record_array,
    })
    return jsonify({
        "status": 200,
    })

@app.route("/route/<id>")
def get_route(id):
    try:
        result = routes.find_one({"_id":ObjectId(id)})
        result = dumps(result)
    except:
        result = "No Data available"
    return result

@app.route("/routes")
def get_routes():
    r = routes.find()
    return jsonify({
        "data": dumps(r)
    })

if __name__ == "__main__":
    app.run(debug=True)
