import json
from pymongo import MongoClient

client = MongoClient('localhost', 27017).greysky.route

def insert_route_database(route):
    try:
        i_id = client.insert_one(route)
        status = 200
    except ValueError:
        status = 500
    return status

def get_all_route_database():
    route = client.find()
    for r in route:
        print(r)
        r["_id"] = str(r["_id"])
    return route

if __name__ == "__main__":
    run()
